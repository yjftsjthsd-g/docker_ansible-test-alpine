# ansible-test-alpine

A docker image built from Alpine Linux to be provisioned by Ansible.

## Prerequisite

This is built on top of `sshd-alpine`, which is publically published here: https://gitlab.com/yjftsjthsd-g/docker_sshd-alpine


## Use

(General suggestions; adjust as needed.)

### Run the container (detached, throwaway):
```
sudo docker run -p 2222:22 -d --rm registry.gitlab.com/yjftsjthsd-g/docker_ansible-test-alpine:latest
```

### Inventory stanza:
```
[localvm]
127.0.0.1 ansible_connection=ssh ansible_ssh_user=root ansible_ssh_port=2222
```

### Manually access the container:
```
ssh -o UserKnownHostsFile=/dev/null  -p 2222 root@localhost
pkill sshd  # kill container from the inside
```

